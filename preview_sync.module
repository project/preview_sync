<?php

/**
 * @file
 * Code for the Preview Sync feature.
 */

include_once 'preview_sync.features.inc';

define('PREVIEW_SYNC_DRUSH_FILE', '/etc/drush/preview.aliases.drushrc.php');
define('PREVIEW_SYNC_QUEUE', 'preview_sync_queue');
define('PREVIEW_SYNC_QUEUE_LEASE_TIME', 1200);
define('PREVIEW_SYNC_REVIEW_STATE', 'needs_review');
define('PREVIEW_SYNC_SUCCESS', 1);
define('PREVIEW_SYNC_ERROR', 0);

/**
 * Implements hook_permission().
 */
function preview_sync_permission() {
  return array(
    'administer preview_sync' => array(
      'title' => t('Administer Preview Sync'),
      'description' => t('Allows administrators to access the Preview Sync page and launch a new syndication.'),
      'restrict access' => TRUE,
    ),
    'clear logs preview_sync' => array(
      'title' => t('Clear logs for Preview Sync'),
      'description' => t('Allows administrators to clear the logs and reset the average task duration.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu()
 */
function preview_sync_menu() {
  $items['admin/reports/bulk-preview'] = array(
    'title' => t('Preview Sync'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('preview_sync_overview'),
    'file' => 'preview_sync.admin.inc',
    'access callback' => 'preview_sync_overview_access',
    'access arguments' => array('administer preview_sync'),
    'description' => t('Allows administrators to see the current status of the Preview Sync, launch new syndications.'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/reports/bulk-preview/start'] = array(
    'title' => t('Preview Sync start'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('preview_sync_start_confirm'),
    'file' => 'preview_sync.admin.inc',
    'access callback' => 'preview_sync_start_access',
    'access arguments' => array('administer preview_sync'),
    'description' => t('Confirm form for starting a new Preview Sync.'),
    'type' => MENU_NORMAL_ITEM,
  );

  // AJAX for live data.
  $items['ajax/preview_sync'] = array(
    'title' => 'Preview Sync data AJAX callback',
    'page callback' => 'preview_sync_ajax',
    'file' => 'preview_sync.ajax.inc',
    'access arguments' => array('administer preview_sync'),
    'description' => 'Gets the most task progress.',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Custom access control to ensure the Preview Sync screen is not viewed on
 * environments with no corresponding preview server.
 */
function preview_sync_overview_access($permission) {
  // @TODO remove hard coded environment here.
  return user_access($permission) && in_array(environment_current(), array('development', 'uat', 'production'));
}

/**
 * Custom access control to ensure the Preview Sync batch is not already started
 * before adding another. This permission check inherits
 * preview_sync_overview_access().
 *
 * @see preview_sync_overview_access().
 */
function preview_sync_start_access($permission) {
  return preview_sync_overview_access($permission) && preview_sync_is_empty();
}

/**
 * Implements hook_theme().
 */
function preview_sync_theme($existing, $type, $theme, $path) {
  $path = drupal_get_path('module', 'preview_sync') . '/templates';
  return array(
    'preview_sync_idle' => array(
      'variables' => array('messages' => array(), 'history' => array()),
      'template' => 'preview-sync-idle',
      'path' => $path,
    ),
    'preview_sync_progress' => array(
      'variables' => array('messages' => array(), 'history' => array()),
      'template' => 'preview-sync-progress',
      'path' => $path,
    ),
  );
}

/**
 * Implements hook_cron().
 */
function preview_sync_cron() {
  // Cleans up the log table, so it rotates every so often.
  $batches_to_keep = variable_get('preview_sync_batch_limit', 10);
  $current_pid = preview_sync_max_pid();
  if ($current_pid > 0) {
    db_delete('preview_sync_log')
      ->condition('pid', $current_pid - $batches_to_keep, '<=')
      ->execute();
  }
}

/**
 * Implementation of hook_environment().
 */
function preview_sync_environment() {
  // Define a preview environment.
  return array(
    'preview' => array(
      'label' => t('Preview'),
      'allowed' => array(
        'default' => FALSE,
      ),
    ),
  );
}

/**
 * Returns the from and the to aliases for the current alias file.
 *
 * Example code is:
 *   list($from, $to) = preview_sync_get_aliases();
 *   $from_alias = reset(array_keys($from));
 *   $to_alias = reset(array_keys($to));
 *
 * @return array
 *   keyed by alias.
 */
function preview_sync_get_aliases() {
  require PREVIEW_SYNC_DRUSH_FILE;
  $from = '';
  $to = '';

  foreach ($aliases as $key => $alias) {
    if ($alias['preview-sync']['to'] === TRUE) {
      $to = $key;
    }
    else if ($alias['preview-sync']['from'] === TRUE) {
      $from = $key;
    }
  }

  if (empty($from) || empty($to)) {
    throw new Exception(t('You are missing a correctly filled out Drush alias file.'));
  }

  return array(
    array(
      $from => $aliases[$from],
    ),
    array(
      $to => $aliases[$to],
    ),
  );
}

/**
 * A list of tasks that makes up a whole Preview Sync.
 *
 * @return array
 *   A list of tasks, with each task having:
 *     - title - human readable name of the task, displayed in the
 *     administration UI.
 *     - command - Drush command, with preference for the full version and not
 *     any aliases.
 *     - arguments - arguments to the Drush command.
 */
function preview_sync_tasks() {
  list($from, $to) = preview_sync_get_aliases();
  list($from_key) = array_keys($from);
  $from_alias = 'preview.' . $from_key;
  list($to_key) = array_keys($to);
  $to_alias = 'preview.' . $to_key;

  $tasks['rsync'] = array(
    'title' => t('Rsync files'),
    'command' => 'core-rsync',
    'site' => "@$from_alias",
    'arguments' => array("@$from_alias:%files", "@$to_alias:%files"),
  );
  $tasks['sql_dump'] = array(
    'title' => t('Dump production database'),
    'command' => 'sql-dump',
    'site' => "@$from_alias",
  );
  $tasks['sql_drop'] = array(
    'title' => t('Drop preview database'),
    'command' => 'sql-drop',
    'site' => "@$to_alias",
  );
  $tasks['sql_import'] = array(
    'title' => t('Import dumped database into preview'),
    'command' => 'sqlc < ' . $from[$from_key]['path-aliases']['%dump'],
    'site' => "@$to_alias",
  );
  $tasks['env_switch'] = array(
    'title' => t('Environment switch'),
    'command' => 'env-switch',
    'site' => "@$to_alias",
    'arguments' => array('preview'),
  );
  $tasks['cache_clear'] = array(
    'title' => t('Cache clear'),
    'command' => 'cache-clear',
    'site' => "@$to_alias",
    'arguments' => array('all'),
  );
  if (module_exists('workbench_moderation')) {
    $tasks['workbench_moderation_publish'] = array(
      'title' => t('Workbench moderation publish'),
      'command' => 'workbench-moderation-publish',
      'site' => "@$to_alias",
    );
  }
  if (module_exists('varnish')) {
    $tasks['varnish'] = array(
      'title' => t('Clear varnish'),
      'command' => 'varnish-purge-all',
      'site' => "@$to_alias",
    );
  }
  if (module_exists('apachesolr')) {
    $tasks['solr_drop'] = array(
      'title' => t('Solr drop'),
      'command' => 'solr-delete-index',
      'site' => "@$to_alias",
    );
    $tasks['solr_mark'] = array(
      'title' => t('Solr mark'),
      'command' => 'solr-mark-all',
      'site' => "@$to_alias",
    );
    $tasks['solr_index'] = array(
      'title' => t('Solr re-index'),
      'command' => 'solr-index',
      'site' => "@$to_alias",
    );
  }

  // Allow other modules to jump in if needed.
  drupal_alter('preview_sync_tasks', $tasks);
  return $tasks;
}

/**
 * What the Drush command ends up looking like on CLI.
 */
function preview_sync_task_to_string($data) {
  $arguments = '';
  $options = '';
  foreach ($data['arguments'] as $key => $value) {
    if (is_numeric($key)) {
      $arguments .= ' ' . $value;
    }
    else {
      $arguments .= ' --' . $key . '=' . $value;
    }
  }
  foreach ($data['options'] as $key => $value) {
    $options .= ' --' . $value;
  }
  return 'drush ' . $data['site'] . ' --nocolor -y ' . $data['command'] . $arguments . $options;
}

/**
 * Returns the number of the presently highest batch in the log, or 0 if there
 * are no log messages.
 *
 * @return int
 */
function preview_sync_max_pid() {
  $current_pid = db_query("SELECT MAX(pid) FROM {preview_sync_log}")->fetchField();
  if (empty($current_pid)) {
    $current_pid = 0;
  }
  return (int) $current_pid;
}

/**
 * Determine whether there is a Preview Sync in progress or not happening now.
 * This is simply a matter of checking the queue for entries in it.
 */
function preview_sync_is_empty() {
  $queue = DrupalQueue::get(PREVIEW_SYNC_QUEUE);
  $queue->createQueue();
  return $queue->numberOfItems() == 0;
}

/**
 * Returns the current status of the tasks in the latest batch.
 *
 * @return array
 */
function preview_sync_get_latest_batch() {
  // Is the queue empty, if so there is no latest batch (only log messages).
  if (preview_sync_is_empty()) {
    return array();
  }

  // Queue is not empty, so we potentially have a mixture of queue items and
  // log messages here, making up the current batch.
  $data_last = db_query_range("SELECT data FROM {queue} WHERE name = :name ORDER BY item_id DESC", 0, 1, array(':name' => PREVIEW_SYNC_QUEUE))->fetchField();
  $data_last = unserialize($data_last);

  // Find the two highest batch IDs.
  $log_latest_pid = preview_sync_max_pid();
  $queue_latest_pid = (int) $data_last['pid'];

  // Get the queue items, as a giant array of arrays.
  $queue_items = db_query("SELECT item_id, data, expire FROM {queue} WHERE name = :name ORDER BY item_id ASC", array(':name' => PREVIEW_SYNC_QUEUE))->fetchAllAssoc('item_id', PDO::FETCH_ASSOC);
  foreach ($queue_items as $index => $item) {
    $queue_items[$index]['data'] = unserialize($item['data']);

    // If the queue item has an expiry, then it is in progress (i.e. the lease
    // has started).
    if ($item['expire'] > 0) {
      $queue_items[$index]['status'] = 2;

      // Lets estimate the remaining time for this queued item.
      $start = $item['expire'] - PREVIEW_SYNC_QUEUE_LEASE_TIME;
      $average_time_to_complete = db_query("SELECT AVG(duration) FROM {preview_sync_log} WHERE type = :type", array(':type' => $queue_items[$index]['data']['type']))->fetchField();

      // We may not have duration for this item.
      if (!empty($average_time_to_complete)) {
        $average_time_to_complete = ceil($average_time_to_complete / 1000);
        $queue_items[$index]['remaining'] = $start + $average_time_to_complete - REQUEST_TIME;
        $queue_items[$index]['remaining_friendly'] = format_interval($queue_items[$index]['remaining']);
      }
      else {
        $queue_items[$index]['remaining'] = t('Unknown');
      }
    }
    // Else, it is waiting to be picked up.
    else {
      $queue_items[$index]['status'] = -1;
    }

    // Default duration to blank.
    $queue_items[$index]['duration_friendly'] = '';
  }

  // Easy case, the log does not contain the latest batch, only in the queue.
  if ($queue_latest_pid > $log_latest_pid) {
    return $queue_items;
  }
  // Next case, we have a mixture of log and queue.
  else if ($queue_latest_pid === $log_latest_pid) {
    $tasks = preview_sync_tasks();
    $log_entries = db_query("SELECT pid, qid, duration, type, status FROM {preview_sync_log} WHERE pid = :pid ORDER BY qid ASC", array(':pid' => $log_latest_pid))->fetchAllAssoc('qid', PDO::FETCH_ASSOC);

    foreach ($log_entries as $id => $log) {
      // Friendly titles.
      $log_entries[$id]['data']['title'] = $tasks[$log['type']]['title'];

      // Consistent status integers.
      $log_entries[$id]['status'] = (int) $log_entries[$id]['status'];

      // Friendly duration.
      $log_entries[$id]['duration_friendly'] = format_interval(ceil($log['duration'] / 1000));
    }

    // Preserve keys.
    return $log_entries + $queue_items;
  }

  // Should never happen.
  else {
    throw new Exception(t('Queued item batch ID [@queue] is less than the log batch ID [@log]. Something is broken.', array(
      '@queue' => $queue_latest_pid,
      '@log' => $log_latest_pid,
    )));
  }
}

/**
 * Get the total amount of content items in "review" broken down by content
 * type.
 *
 * @return array
 *   Array of content items, keyed by content type.
 */
function preview_sync_content_in_review() {
  $nodes = db_query("SELECT count(nh.nid) AS count, n.type
                     FROM {workbench_moderation_node_history} nh
                     LEFT JOIN {node} n
                       ON n.nid = nh.nid
                     WHERE nh.state = :state
                       AND nh.current = 1
                     GROUP BY n.type", array(':state' => PREVIEW_SYNC_REVIEW_STATE))->fetchAllAssoc('type');

  $total = 0;
  foreach ($nodes as $node) {
    $total += $node->count;
  }

  return array('total' => $total, 'nodes' => $nodes);
}

/**
 * Finds the content to publish that is currently in review.
 *
 * @param integer $offset
 *   0 indexed integer to offset the start of the first record.
 * @param integer $limit
 *   The maximum number of records to return.
 *
 * @return array
 *   Array of node ID and revision IDs for the content.
 */
function preview_sync_get_review_content($offset = 0, $limit = 10) {
  $nodes = array();
  $query = db_query_range("SELECT nh.nid, nh.vid
                           FROM {workbench_moderation_node_history} nh
                           WHERE nh.state = :state
                             AND nh.current = 1
                           ORDER BY nh.nid ASC", $offset, $limit, array(':state' => PREVIEW_SYNC_REVIEW_STATE));

  foreach ($query as $row) {
    $node = array(
      'nid' => $row->nid,
      'vid' => $row->vid
    );
    $nodes[] = $node;
  }

  return $nodes;
}
